﻿param(
  [parameter(Mandatory)]
  [string] $identityKeyFile
)

# ssh-agentサービスが停止されていたら起動する
$service = Get-Service ssh-agent -ErrorAction SilentlyContinue
if($?){
    if($service.status -eq "Stopped"){
        Start-Service ssh-agent -ErrorAction SilentlyContinue
        if($?){
            echo "ssh-agentサービスの起動に成功しました"
        }else{
            echo "ssh-agentサービスの起動に失敗しました。サービスが無効になっているかも"
            exit -1
        }
    }else{
        echo "ssh-agentサービスは起動済み"
    }
}else{
    echo "ssh-agentサービスが存在してないかも"
}

# 秘密鍵がssh-agentに未登録の場合は登録する
$sshKeyList = ssh-add -L
if($?){
    echo "秘密鍵はssh-agentに登録済み"
    #echo "削除したい場合は「ssh-add -D」を実行するか、再起動してください"
}else{
    echo "秘密鍵をssh-agentに登録します"
    ssh-add $identityKeyFile
}
