# SshKeyAdd

## 処理内容

- ssh-agent を起動し、秘密鍵が未登録の場合は登録する

## 事前準備

- 環境変数を作成
  - <変数名>GIT_SSH
  - <変数値>C:\Windows\System32\OpenSSH\ssh.exe
- サービスを無効から自動にする
  - windows ボタン + R
  - 「services.msc」を入力し Enter
  - 「OpenSSH Authentication Agent」が無効である場合、
    右クリック → プロパティ → スタートアップの種類を「自動」にする
- run.bat に記載している「identityKeyFile」の設定値に秘密鍵へのフルパスを記載する

## 実行方法

- run.bat を実行する。パスワードを聞かれたら秘密鍵を作った時のパスワードを入力する
