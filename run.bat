@echo off
SET curDir=%~dp0
REM 秘密鍵のフルパス
SET identityKeyFile=%homedrive%%homepath%\.ssh\id_rsa

powershell -NoProfile -ExecutionPolicy RemoteSigned -File %curDir%sshAdd.ps1^
    -identityKeyFile %identityKeyFile%

pause